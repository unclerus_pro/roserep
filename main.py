# -*- coding: utf-8 -*-
import logging
from jira import JIRA
import dateutil.parser
import argparse


def dump_fields(f):
    return ['%s = %s' % (attr, getattr(f, attr)) for attr in dir(f) if not attr.startswith('__')]


class Command:

    JIRA_URL = 'https://biopic.atlassian.net'

    def __init__(self, auth):
        self.log = logging.getLogger(type(self).__name__)
        self.out = {}
        self.log.info('Авторизация на %s под пользователем %s...', self.JIRA_URL, auth[0])
        self.jira = JIRA(self.JIRA_URL, basic_auth=auth)


class Report:

    def __init__(self, builder):
        self.builder = builder
        self.raw = None
        self.title = None

    def build(self):
        raise NotImplementedError()

    def dump(self):
        raise NotImplementedError()


class StatusesReport(Report):

    def __init__(self, *args, **kwargs):
        super(StatusesReport, self).__init__(*args, **kwargs)
        self.title = 'Отчет по текущим статусам задач'

    def build(self):
        self.raw = {}
        self.builder.log.info('Построение отчета по текущим статусам задач...')

        for key, t in self.builder.tasks.items():
            status = str(t.fields.status)
            count = self.raw.get(status, None)
            if not count:
                self.raw[status] = [0, []]
            self.raw[status][0] += 1
            self.raw[status][1].append(key)

        self.builder.log.info('Готово')

    def dump(self):
        for status, (count, tasks) in self.raw.items():
            print("%s: %d (%s)" % (status, count, ', '.join(tasks)))


class SimulReport(Report):

    __STATUSES = (
        'DEVELOPMENT', 'ANALYSIS', 'В работе'
    )

    def __init__(self, *args, **kwargs):
        super(SimulReport, self).__init__(*args, **kwargs)
        self.title = 'Параллельная работа над задачами'

    def build(self):
        self.raw = {}
        self.builder.log.info('Построение отчета по параллельной работе...')

        buf = {}
        for key, t in self.builder.tasks.items():
            dev = str(t.fields.assignee)
            if dev not in buf:
                buf[dev] = []
            if str(t.fields.status) in self.__STATUSES:
                buf[dev].append(key)

        for dev, tasks in buf.items():
            if len(tasks) > 1:
                self.raw[dev] = tasks

        self.builder.log.info('Готово')

    def dump(self):
        for dev, tasks in self.raw.items():
            print('%s;%s' % (dev, ', '.join(tasks)))


class SlipReport(Report):

    def __init__(self, *args, **kwargs):
        super(SlipReport, self).__init__(*args, **kwargs)
        self.title = 'Задачи со сдвигом дедлайна'

    def build(self):
        self.raw = {}
        self.builder.log.info('Построение списка задач со сдвигом дедлайна')

        for key, t in self.builder.tasks.items():
            self.raw[key] = []
            for h in t.changelog.histories:
                for i in h.items:
                    if i.field == 'duedate':
                        self.raw[key].append((str(h.author), str(h.created), getattr(i, 'from'), i.to))
                        break

        self.builder.log.info('Готово')

    def dump(self):
        for key, changes in self.raw.items():
            if len(changes) in (0, 1):
                continue
            print('%s: сдвигов дедлайна %d' % (key, len(changes) - 1))
            for author, date, old, new in sorted(changes, key=lambda c: c[1]):
                if old is not None:
                    print('%s: %s -> %s (%s)' % (dateutil.parser.isoparse(date).strftime('%d.%m.%Y'), old, new, author))


class ChangeStatusReport(Report):

    __CUSTOM_FIELDS = {
        'ERP': {
            'product': 'customfield_10147',
            'qa': 'customfield_10152',
        },
        'BPMDEV': {
            'product': 'customfield_10150',
            'qa': 'customfield_10151',
        }
    }

    def __init__(self, statuses, *args, **kwargs):
        super(ChangeStatusReport, self).__init__(*args, **kwargs)
        self.status, self.real_status = statuses
        self.title = 'Отчет "Ушло в %s"' % self.status

    @staticmethod
    def get_dev(task):
        return str(task.fields.assignee)

    def get_product(self, task):
        field = self.__CUSTOM_FIELDS.get(self.builder.board, {}).get('product', None)
        if field:
            res = getattr(task.fields, field, '')
            return res.value if res else ''
        return ''

    def get_qa(self, task):
        field = self.__CUSTOM_FIELDS.get(self.builder.board, {}).get('qa', None)
        if field:
            res = getattr(task.fields, field, '')
            if res:
                return res.value
        for h in task.changelog.histories:
            for i in h.items:
                if i.fromString and i.toString and \
                        i.fromString.lower() == 'ready to qa' and i.toString.lower() == 'done':
                    return str(h.author)
        return ''

    def build(self):
        self.builder.log.info('Построение списка "Ушло в %s"...', self.status)
        self.raw = {}
        for key, t in self.builder.tasks.items():
            if str(t.fields.status) != self.status:
                continue
            for h in t.changelog.histories:
                dt_done = None
                for i in h.items:
                    if i.toString == self.real_status:
                        dt_done = dateutil.parser.isoparse(h.created)
                        break
                if dt_done:
                    self.raw[dt_done] = {
                        'datetime': dt_done,
                        'task': key,
                        'developer': self.get_dev(t),
                        'product': self.get_product(t),
                        'qa': self.get_qa(t),
                    }
                    break

        self.builder.log.info('Готово')

    def dump(self):
        for date in sorted(self.raw.keys()):
            print(';'.join((
                date.strftime('%d.%m.%Y'),
                self.raw[date]['task'],
                str(self.raw[date]['developer']),
                str(self.raw[date]['product']),
                str(self.raw[date]['qa']),
            )))


class ReportBuilder(Command):

    BOARDS = {
        'ERP': {
            'done': ('Готово', 'Done'),
            'qa': ('Ready to QA', 'Ready to QA'),
        },
        'BPMDEV': {
            'done': ('Готово', 'Done'),
            'qa': ('READY TO QA', 'READY TO QA'),
        }
    }

    def __init__(self, auth, board, sprint):
        super(ReportBuilder, self).__init__(auth)
        if board not in self.BOARDS:
            raise KeyError('Unknown board')
        self.board = board
        self.sprint = '(%s)' % ', '.join("'#%d'" % s for s in sprint) if sprint else 'openSprints()'
        self.tasks = {}
        self.reports = [
            StatusesReport(self),
            ChangeStatusReport(self.BOARDS[board]['done'], self),
            ChangeStatusReport(self.BOARDS[board]['qa'], self),
            SimulReport(self),
            SlipReport(self),
        ]

    def fetch_tasks(self):
        self.log.info('Получение списка задач с доски %s в спринтах %s...', self.board, self.sprint)
        self.tasks.clear()
        for i in self.jira.search_issues(
                'project=%s and sprint in %s and issuetype != Подзадача' % (self.board, self.sprint),
                maxResults=0, expand='names,changelog'):
            self.tasks[i.key] = i
        self.log.info('Получено задач: %d', len(self.tasks))

    def run(self):
        self.fetch_tasks()
        for report in self.reports:
            report.build()

    def dump(self):
        for report in self.reports:
            print('*' * 80)
            print(report.title)
            print('*' * 80)
            report.dump()
            print()


def parse_args():
    parser = argparse.ArgumentParser(description='Статистические отеты в jira')
    parser.add_argument('-u', '--user', help='Имя пользователя JIRA', required=True)
    parser.add_argument('-k', '--key', help='Ключ API', required=True)
    parser.add_argument('-p', '--project', help='Имя проекта (доски)', required=True,
                        choices=ReportBuilder.BOARDS.keys())
    parser.add_argument('-s', '--sprint', help='Номера спринтов (если аргумент не указан, то открытые)',
                        type=int, nargs='*', default=None)
    parser.add_argument('-v', '--verbose', help='Уровень логгирования',
                        choices=(0, 1, 2), type=int, default=0)
    return parser.parse_args()


def setup_log(level):
    if level < 1:
        return
    level = logging.INFO if level < 2 else logging.DEBUG
    console = logging.StreamHandler()
    console.setLevel(level)
    logging.getLogger('').setLevel(level)
    logging.getLogger('').addHandler(console)


class ArgumentError(Exception):
    pass


def main():
    args = parse_args()
    setup_log(args.verbose)

    cmd = ReportBuilder((args.user, args.key), args.project, args.sprint)
    cmd.run()
    cmd.dump()


if __name__ == '__main__':
    main()
